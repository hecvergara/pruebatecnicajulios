# language: es
Característica: Autenticacion y compra de articulos

  Escenario: Comprar un articulo
    Dado que Juan ingresa a la plataforma de compras
    Cuando se autentique en la plataforma
    Y agregue algun articulo al carro y haga la compra
    Entonces la compra se realizara correctamente