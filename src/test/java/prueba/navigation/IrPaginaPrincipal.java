package prueba.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

/***
 * Si no se indica desde el comando de ejecucion cual pagina cargar, por defecto carga la que se encuentra en el tag
 * @DefaultUrl
 */
@DefaultUrl("https://www.saucedemo.com")
public class IrPaginaPrincipal extends PageObject {
}
