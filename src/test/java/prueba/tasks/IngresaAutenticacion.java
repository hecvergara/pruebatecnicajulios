package prueba.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import prueba.userinterface.Login;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IngresaAutenticacion implements Task {
    public final String username;
    public final String password;

    public IngresaAutenticacion(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static Performable credenciales(String username, String password) {
        return instrumented(IngresaAutenticacion.class, username, password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(username).into(Login.USERNAME_TXT),
                Enter.theValue(password).into(Login.PASSWORD_TXT),
                Click.on(Login.LOGIN_BTN));

    }
}
