package prueba.tasks;

import com.github.javafaker.Faker;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import prueba.userinterface.Comprar;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class HaceCompra implements Task {

    public static Performable agregarAlCarrito() {
        return instrumented(HaceCompra.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(Comprar.ADDTOCART_BTN),
                Click.on(Comprar.CARRITO_IMG),
                Click.on(Comprar.CHECKOUT_BTN),
                Enter.theValue(new Faker().name().firstName()).into(Comprar.FIRSTNAME_TXT),
                Enter.theValue(new Faker().name().lastName()).into(Comprar.LASTNAME_TXT),
                Enter.theValue(new Faker().address().zipCode()).into(Comprar.POSTALCODE_TXT),
                Click.on(Comprar.CONTINUE_BTN),
                Click.on(Comprar.FINISH_BTN)
        );
    }
}
