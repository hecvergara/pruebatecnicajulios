package prueba.stepdefinition;

import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import prueba.navigation.NavegarA;
import prueba.questions.MensajeCompraExitoso;
import prueba.tasks.HaceCompra;
import prueba.tasks.IngresaAutenticacion;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.IsEqual.equalTo;

public class StepCompraArticulo {
    String actor;
    private String mensajeEsperado = "THANK YOU FOR YOUR ORDER";

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que (.*?) ingresa a la plataforma de compras$")
    public void queIngresaALaPlataformaDeCompras(String actor) {
        this.actor = actor;
        theActorCalled(actor).attemptsTo(NavegarA.paginaPrincipalCompras());
    }

    @Cuando("^se autentique en la plataforma$")
    public void seAutentiqueEnLaPlataforma() {
        theActorCalled(actor).attemptsTo(IngresaAutenticacion.credenciales("standard_user", "secret_sauce"));

    }

    @Y("^agregue algun articulo al carro y haga la compra$")
    public void agregueAlgunArticuloAlCarroDeCompras() {
        theActorCalled(actor).attemptsTo(HaceCompra.agregarAlCarrito());
    }

    @Entonces("^la compra se realizara correctamente$")
    public void laCompraSeRealizaraCorrectamente() {
        theActorInTheSpotlight().should(
                seeThat("El mensaje despues de hacer la compra", MensajeCompraExitoso.mensajeCompraRealizada()
                        , equalTo(mensajeEsperado))
        );

    }
}
